# RelativePopupWindow
Android PopupWindow that can be easily located relative to anchor View.

![Art](art/art1.gif)

## How To Use

```java
popup.showOnAnchor(anchor, VerticalPosition.ABOVE, HorizontalPosition.CENTER);
```

See detail example in [ExampleCardPopup.kt](example/src/main/kotlin/com/labo/kaji/relativepopupwindow/example/ExampleCardPopup.kt)
or in Java, [ExampleCardPopup.java](example/src/main/java/com/labo/kaji/relativepopupwindow/example/java/ExampleCardPopup.java).

## Position Parameters

### VerticalPosition

- ABOVE
- ALIGN_BOTTOM
- CENTER
- ALIGN_TOP
- BELOW

### HorizontalPosition

- LEFT
- ALIGN_RIGHT
- CENTER
- ALIGN_LEFT
- RIGHT

### Fit In Screen

If you want Popup not to fit in screen automatically, add parameter to disable it.

```java
popup.showOnAnchor(anchor, VerticalPosition.ABOVE, HorizontalPosition.CENTER, false);
```

## Install

This library is available in jcenter.

- for AndroidX:

```groovy

```

- for legacy Support Library:

```groovy

```
